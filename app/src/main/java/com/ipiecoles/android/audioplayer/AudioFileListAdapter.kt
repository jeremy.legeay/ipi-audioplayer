package com.ipiecoles.android.audioplayer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ipiecoles.android.audioplayer.databinding.AudioFileItemBinding

class AudioFileListAdapter(
    private val fileList: List<AudioFile>
): RecyclerView.Adapter<AudioFileListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding = AudioFileItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }
    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {
        val file = fileList[position]
        holder.bind(file)
    }
    override fun getItemCount(): Int = fileList.size

    inner class ViewHolder(
        private val binding: AudioFileItemBinding
    ): RecyclerView.ViewHolder(binding.root) {
        fun bind(audioFile: AudioFile) {
            binding.title.text = audioFile.title
            binding.artist.text = audioFile.artist
            binding.album.text = audioFile.album
            binding.duration.text = audioFile.durationText
        }
    }
}