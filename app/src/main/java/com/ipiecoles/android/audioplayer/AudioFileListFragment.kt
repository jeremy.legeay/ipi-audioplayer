package com.ipiecoles.android.audioplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ipiecoles.android.audioplayer.databinding.FragmentAudioFileListBinding

class AudioFileListFragment: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentAudioFileListBinding.inflate(
            inflater,
            container,
            false
        )
        binding.audioFileList.layoutManager =
            LinearLayoutManager(binding.root.context)

        val fakeList = listOf(AudioFile(1, "Titre1", "Artiste1", "Album1", 1901), AudioFile(2, "Titre2", "Artiste2", "Album2", 1902))
        binding.audioFileList.adapter = AudioFileListAdapter(fakeList)

        return binding.root
    }
}
