package com.ipiecoles.android.audioplayer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showAppFragment()
    }
    fun showAppFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        val fragment = AudioFileListFragment()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.commit()
    }
}